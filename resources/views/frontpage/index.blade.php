@extends('frontpage.container')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Posts #1</div>
                <div class="card-body">
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Amet molestiae excepturi eveniet non voluptate, quos saepe laborum illum aperiam possimus officiis eligendi dignissimos commodi quibusdam quisquam nulla similique nobis praesentium!
                </div>
            </div>
        </div>
        <div class="col-md-8 mt-3">
            <div class="card">
                <div class="card-header">Posts #1</div>
                <div class="card-body">
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Amet molestiae excepturi eveniet non voluptate, quos saepe laborum illum aperiam possimus officiis eligendi dignissimos commodi quibusdam quisquam nulla similique nobis praesentium!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection